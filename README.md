# WWU Drupal Composer Installation
These instructions will yield an environment suitable for theme development following the instructions in the [Ashlar](https://bitbucket.org/wwuweb/ashlar/src/master/) repository.

## Getting Started

### I. Lando
Download and install the latest **stable** [version of Lando](https://github.com/lando/lando/releases). The latest version to be tested with these instructions is [v3.0.0-rc.23](https://github.com/lando/lando/releases/tag/v3.0.0-rc.14)

### II. Create a new lando workspace

0. Clone this repository.

0. Run the following commands:

        git clone https://bitbucket.org/wwuweb/wwu_drupal_local.git
        cd wwu_drupal_local
        lando start

This will create a workspace with the default name "my-new-site". If you would like to have multiple Lando containers running simultaneously, or would like to control the URL of your lando instance, edit the first line of the .lando.yml file with a different site name.

Lando will automatically run composer install, so just sit back and wait for the install to complete. It should take a minute or two.

### III. Finalize Drupal Install

In your browser go to http://my-new-site.lndo.site.

0. You will see a Choose your language screen. Choose a language and click continue.

0. Select WWU Drupal for the install profile.

0. Fill in the database credentials as follows:

        Database type: MySQL, MariaDB, Percona Server, or equivalent
        Database name: drupal8
        Database username: drupal8
        Database password: drupal8

  *Under advanced options:*

        Host: database
        Port number: 3306

0. Click save and continue.

0. Wait while the site installs.

0. Fill in the site configuration page. Use whatever values you want as long as you can remember them. Click save and continue.

### IV. Compile and Enable Ashlar

    cd web/themes/contrib/ashlar

    lando npm install gulp
    lando gulp
    lando drush cr

Navigate to the site.

Your new site should now be ready for development.

If you would like to develop on the theme, follow the instructions [on the main Ashlar repository](https://bitbucket.org/wwuweb/ashlar/src) starting at step **II. Ashlar**.

## Troubleshooting

If you are having any issues with your Lando box (for example, gulp commands won't run) try the following:

    lando rebuild

This is a non destructive way of resetting the Lando container. If this doesn't work, you may need to destroy and restart your box. You can do this without losing your database (and having to start from scratch) by first exporting and then reimporting the database, as follows:

    lando db-export dump.sql
    lando destroy -y
    lando start
    lando db-import dump.sql.gz

## Updating Drupal Core

This project will attempt to keep all of your Drupal Core files up-to-date; the project [drupal-composer/drupal-scaffold](https://github.com/drupal-composer/drupal-scaffold) is used to ensure that your scaffold files are updated every time drupal/core is updated. If you customize any of the "scaffolding" files (commonly .htaccess), you may need to merge conflicts if any of your modified files are updated in a new release of Drupal core.

Run `composer update drupal/core webflo/drupal-core-require-dev symfony/* --with-dependencies` to update Drupal Core and its dependencies.
